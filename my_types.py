import argparse
from fractions import Fraction


def positive_float(_x):
    try:
        x = float(_x)
        if x <= 0.0:
            raise argparse.ArgumentTypeError("%s является недопустимым положительным значением" % x)
    except ValueError:
        try:
            x = Fraction(_x)
            x = x.numerator / x.denominator
            if x <= 0.0:
                raise argparse.ArgumentTypeError("%s является недопустимым положительным значением" % x)
        except ValueError:
            raise argparse.ArgumentTypeError("%r не литерал с плавающей точкой" % (_x,))
    return x


def restricted_float(_x):
    try:
        x = float(_x)
    except ValueError:
        try:
            x = Fraction(_x)
            x = x.numerator / x.denominator
        except ValueError:
            raise argparse.ArgumentTypeError("%r не литерал с плавающей точкой" % (_x,))
    return x


def percent_float(x):
    try:
        x = float(x)
        if x < 0.0 or x > 1.0:
            raise argparse.ArgumentTypeError("%s является недопустимым положительным значением" % x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r не литерал с плавающей точкой" % (x,))
    return x


def restricted_int(x):
    try:
        x = int(x)
        if x <= 0:
            raise argparse.ArgumentTypeError("%s является недопустимым положительным значением" % x)
    except ValueError:
        raise argparse.ArgumentTypeError("%r не целочисленный литерал" % (x,))
    return x


def required_length(true_len):
    class RequiredLength(argparse.Action):
        def __call__(self, parser, args, values, option_string=None):
            if len(values) != true_len:
                msg = 'argument "{f}" requires {true_len} arguments'.format(f=self.dest, true_len=true_len)
                raise argparse.ArgumentTypeError(msg)
            setattr(args, self.dest, values)

    return RequiredLength
