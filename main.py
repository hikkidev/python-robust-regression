from scipy.stats import median_absolute_deviation
import argparse
import matplotlib.pyplot as plt
# import pandas as pd

import numpy as np
from tabulate import tabulate

from my_types import positive_float, restricted_int, percent_float, restricted_float, required_length

mad_const = 0.67449  # Check - Median absolute deviation
exp_const = np.exp(1)

params = {'axes.labelsize': 16,
          'axes.titlesize': 16,
          'legend.fontsize': 16,
          'figure.figsize': (16, 9)}
plt.rcParams.update(params)


def f(x):
    return np.array([np.ones(len(x)), np.array(x), np.array(x * x), np.array(x * x * x)]).T


def w(z, c):
    return np.power(exp_const, -c * np.abs(z))


def mean_squared_error(y, y_hat):
    return np.square(y_hat - y).mean()


def error(theta, theta_est):
    diff = theta_est - theta
    return np.dot(diff.T, diff)


def generate_dataset(size, noise_percent, sigma=1.0, scale=15, verbose=False):
    lb = -5
    hb = 10
    exception_count = 0
    theta = np.array([7, 0.5, 0.3, -0.1])
    x = np.linspace(lb, hb, size)
    y = np.dot(f(x), theta)
    y_hat = y.copy()

    if noise_percent > 0:
        exception_count = int(np.round(noise_percent * size))
        gaussian_noise = np.random.normal(0, sigma, size)
        for pos in np.random.choice(size, size=exception_count, replace=False):
            gaussian_noise[pos] *= scale
        y_hat = y_hat + gaussian_noise

    if verbose:
        print("Размер выборки равен: {}".format(size))
        print("Сгенерировано выбросов: {}".format(exception_count))
    return theta, x, y, y_hat


def IRLS(x, y, theta, sigma, c, delta, max_iter=1000):
    X = f(x)
    diagW = []
    iters = 0
    for i in range(max_iter):
        r = y - np.dot(X, theta)
        diagW = w(r / sigma, c)
        W = np.diag(diagW)

        theta_next = np.dot(np.linalg.inv(X.T.dot(W).dot(X)), (X.T.dot(W).dot(y)))
        sigma = median_absolute_deviation(r) / mad_const

        max_theta_diff = np.max(np.abs((theta_next - theta) / theta))
        theta = theta_next

        if max_theta_diff < delta:
            iters = i + 1
            break

    return theta, sigma, diagW, iters


def single_exec(x, y_true, y_noise, c, theta, sigma, delta, noise, plot):
    theta_hat, sigma_hat, diagW, iters = IRLS(x=x, y=y_noise, theta=theta, sigma=sigma, c=c, delta=delta)
    y_hat = np.dot(f(x), theta_hat)

    mse = mean_squared_error(y_true, y_hat)
    theta_error = error(theta, theta_hat)

    if plot:
        plt.figure()
        plt.title("С={}, Зашумленность {:.0%}".format(c, noise))
        plt.scatter(x, y_noise, c='b', alpha=0.35, label="Наблюдения")
        plt.plot(x, y_true, 'r', lw=2, label='Исходная функция', alpha=0.8)
        plt.plot(x, y_hat, 'b', lw=2, label='Функция регрессии')
        plt.legend()
        plt.show()
    return theta_hat, sigma_hat, mse, theta_error, diagW, iters


def main(c_array, theta_init, sigma_init, delta, size, noise_percent, verbose, plot):
    table = []
    hidden_results = []

    theta_true, x, y_true, y_noise = generate_dataset(size, noise_percent, sigma=0.5, verbose=verbose, scale=25)

    if not theta_init:
        theta_init = theta_true
    if not sigma_init:
        sigma_init = np.median(y_noise)

    if verbose:
        # print(tabulate(y_noise.reshape(-1, 1), ["Y"], floatfmt='.3f'))
        print('sigma_init = {}'.format(sigma_init))
        print('theta_init = {}'.format(theta_init))
        print('Среднее = {}'.format(y_noise.mean()))

    for arg_c in c_array:
        theta_hat, sigma_hat, mse, theta_error, diagW, iters = single_exec(x, y_true, y_noise, arg_c, theta_init,
                                                                           sigma_init, delta,
                                                                           noise_percent,
                                                                           plot)
        table.append([arg_c, theta_hat, mse, theta_error, iters])
        hidden_results.append([diagW, sigma_hat])

    header = ["C", "Theta_Hat", "MSE", "Theta_Error", "Iters"]
    print("\nOriginal thetas:", theta_true)
    print(tabulate(table, header, tablefmt="fancy_grid", floatfmt=('g', '', '.5e', '.7e',)))

    print('\nЛучшие параметры модели:')
    best_idx = table.index(min(table, key=lambda arg: arg[3]))
    theta_hat = table[best_idx][1]

    print("C =", table[best_idx][0])
    print("Theta_Hat:", theta_hat)
    print("Sigma_Hat:", hidden_results[best_idx][1])

    if verbose:
        y_hat = np.dot(f(x), theta_hat)
        data = np.array([hidden_results[best_idx][0],
                         y_noise,
                         y_hat,
                         (y_hat - y_noise)
                         ]).T
        print(tabulate(data, ["w", "y", "y_hat", "diff"], tablefmt="fancy_grid",
                       floatfmt=('.3e', '.3f', '.3f', '.7e',)))
        # df = pd.DataFrame(data=data, columns=["w", "y", "y_hat", "diff"])
        # df['w'] = df['w'].map(lambda param: '{:.3e}'.format(param))
        # df['y'] = df['y'].map(lambda param: '{:.3f}'.format(param))
        # df['y_hat'] = df['y_hat'].map(lambda param: '{:.3f}'.format(param))
        # df['diff'] = df['diff'].map(lambda param: '{:.3e}'.format(param))
        # df.to_excel("./out.xlsx")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-c', required=True, type=positive_float, nargs='+', help='Коэффициент(ы) C.')
    parser.add_argument('--size', type=restricted_int, help='Размер выборки. (default: %(default)s)', default=64)
    parser.add_argument('--noise-percent', type=percent_float, help='Процент зашумленности (default: %(default)s).',
                        default=0.0)
    parser.add_argument('--theta', type=restricted_float, nargs='*',
                        help='Начальное приближение theta. (default: Theta Ист.)', default=None,
                        action=required_length(4))
    parser.add_argument('--sigma', type=positive_float, help='Начальная оценка sigma. (default: Медиана Y_Noise)',
                        default=None)
    parser.add_argument('--delta', type=positive_float,
                        help='Условие сходимости для thetas в ИМНК. (default: %(default)s)',
                        default=0.05)

    parser.add_argument('-v', '--verbose', action='store_true', help='Включить расширенный лог. (default: %(default)s)')
    parser.add_argument('-p', '--plot', action='store_true', help='Визуализировать результаты. (default: %(default)s)')
    parser.add_argument('--seed', type=restricted_int,
                        help='Задает начальные условия для генератора случайных чисел. (default: %(default)s)',
                        default=69)
    args = parser.parse_args()

    np.random.seed(args.seed)

    main(args.c, args.theta, args.sigma, args.delta, args.size, args.noise_percent, args.verbose, args.plot)
